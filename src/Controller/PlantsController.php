<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * Plants Controller
 *
 * @property \App\Model\Table\PlantsTable $Plants
 * @method \App\Model\Entity\Plant[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PlantsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Infolinks');
    }

    public function beforeFilter( \Cake\Event\EventInterface $event )
    {
        parent::beforeFilter($event);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->Authorization->skipAuthorization();
        
        $key = $this->request->getQuery('key');
//        exit($key);
        if ( $key ) {
            
            $query = $this->Plants->find('all')->where(['OR' => [
                    ['Plants.name LIKE' => '%' . $key. '%'],
                    ['Plants.trivialname LIKE' => '%' . $key. '%'],
                    ['Families.name LIKE' => '%' . $key. '%'],
                    ['Families.trivialname LIKE' => '%' . $key. '%'],
                    ['Genera.name LIKE' => '%' . $key. '%'],
                    ['Genera.trivialname LIKE' => '%' . $key. '%'],
                ],
            ]);
        } else {
            
            $query = $this->Plants;
        }
        $this->paginate = [
            'contain' => [
                'Families',
                'Genera',
            ],
            'order' => [
                'name' => 'asc',
            ],
        ];
        $plants = $this->paginate($query, [
            'sortableFields' => [
                'name', 
                'trivialname', 
                'indoor', 
                'Families.name', 
                'Genera.name',
            ]
        ]);

        $tblitems = [
            [
                'name' => 'Botanischer Name',
                'field' => [
                    'name' => 'name',
                ],
                'sort' => 'name',
            ],
            [
                'name' => 'Trivialname',
                'field' => [
                    'name' => 'trivialname',
                ],
                'sort' => 'trivialname',
            ],
            [
                'name' => 'Image',
                'field' => [
                    'name' => 'image',
                    'type' => 'imagelink',
                    'alt' => 'name',
                    'class' => 'img-thumbnail rounded float-start tblpic',
                    'dir' => 'plants',
                    'link' => [
                        'controller' => 'Plants',
                        'action' => 'view',
                        'param' => 'id',
                    ]
                ],
            ],
            [
                'name' => 'Indoor?',
                'field' => [
                    'name' => 'indoor',
                    'type' => 'checkbox',
                ],
                'sort' => 'indoor',
            ],
            [
                'name' => 'Family',
                'field' => [
                    'name' => 'family_id',
                    'condition' => 'family',
                    'type' => 'link',
                    'link' => [
                        'controller' => 'Families',
                        'action' => 'view',
                        'linktext' => 'name',
                        'zusatz' => 'trivialname',
                    ]
                ],
                'sort' => 'Families.name',
            ],
            [
                'name' => 'Plant Genus',
                'field' => [
                    'name' => 'genus_id',
                    'condition' => 'genus',
                    'type' => 'link',
                    'link' => [
                        'controller' => 'Genera',
                        'action' => 'view',
                        'linktext' => 'name',
                        'zusatz' => 'trivialname',
                    ]
                ],
                'sort' => 'Genera.name',
            ],
            [
                'name' => 'Actions',
                'class' => 'actions',
                'actionitems' => [
                    [
                        'action' => 'edit',
                        'param' => 'id',
                    ],
                    [
                        'action' => 'delete',
                        'param' => 'id',
                        'postlink' => __('Are you sure you want to delete?'),
                    ],
                ],
            ],
        ];

        $sidenavitems = [
            ['action' => 'add'],
        ];
        $this->set(compact('sidenavitems', 'plants', 'tblitems'));
    }

    /**
     * View method
     *
     * @param string|null $id Plant id.
     *
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = NULL)
    {
        $this->Authorization->skipAuthorization();

        try {
            $plant = $this->Plants->get($id, [
                'contain' => [
                    'Families',
                    'Genera',
                ],
            ]);
        } catch (RecordNotFoundException $e) {
            
            $this->Flash->info(__('Plant do not exist.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
            return $this->redirect(['action' => 'index']);
        }
                
        
        $items = [
            [
                'label' => 'Trivialname',
                'type'  => 'text',
                'text' => $plant->trivialname,
            ],
            [
                'label'      => 'Family',
                'type'       => 'text',
                'text'       => $plant->family->label,
                'link'      => ['controller' => 'Families', 'action' => 'view', $plant->family_id]
            ],
            [
                'label'      => 'Genus',
                'type'       => 'text',
                'text'       => $plant->genus->label,
                'link'      => ['controller' => 'Genera', 'action' => 'view', $plant->genus_id]
            ],
            [
                'label' => 'Indoor?',
                'type'  => 'checkbox',
                'text' => $plant->indoor,
            ],
            [
                'label' => 'Description',
                'type'  => 'textarea',
                'text' => $plant->comment,
            ],
            [
                'text' => 'plants/' . $plant->image,
                'type'  => 'image',
                'alt' => $plant->name,
                'class' => 'img-thumbnail rounded float-start plantpic',
            ],
        ];

        $sidenavitems = [
            [
                'name' => 'List all Plants',
                'action' => 'index',
            ],
            [
                'action' => 'edit',
                'param' => $id,
            ],
            [
                'action' => 'delete',
                'param' => $id,
                'postlink' => __('Are you sure you want to delete {0}?', $plant->name),
            ],
        ];

        $tblitems = [
            [
                'name' => 'Link',
                'field' => [
                    'name' => 'name',
                    'type' => 'externallink',
                    'linktext' => 'comment',
                ],
                'sort' => 'comment',
            ],
        ];
        $infolinks = $this->paginate($this->Infolinks->find()->where(['plant_id' => $id]), ['model' => 'Infolinks']);
        $formtitle = $plant->label;

        $this->set(compact('sidenavitems','plant', 'items', 'tblitems', 'infolinks', 'formtitle'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     *
     * https://discourse.cakephp.org/t/cascading-dropdowns-in-cakephp-4/7160/10
     */
    public function add()
    {
        $plant = $this->Plants->newEmptyEntity();

        $this->Authorization->authorize($plant);
        
        if ( $this->request->is('post') ) {

            $plant = $this->Plants->patchEntity($plant, $this->request->getData());

            if ( !$plant->getErrors() ) {

                $path = WWW_ROOT . 'img' . DS . 'plants';
                $image = $this->request->getData('image_file');
                $filename = $image->getClientFilename();


                if ( strlen($filename) > 0 ) {

                    if ( !is_dir($path) ) {
                        mkdir($path, 0755);
                    }
                    $targetPath = $path . DS . $filename;

                    if ( $image ) {
                        $image->moveTo($targetPath);
                    }
                    $plant->image = $filename;
                }
            }

//            debug($filename);
//            exit;
            if ( $this->Plants->save($plant) ) {

                $this->Flash->info(__('The plant has been saved.'), [
                    'clear'  => TRUE,
                    'params' => ['typ' => 'success',],
                ] );
                return $this->redirect([
                    'action'     => 'index',
                ]);
            } else {

                $this->Flash->info( __('The plant could not be saved. Please, try again.'), [
                    'clear'  => TRUE,
                    'params' => ['typ' => 'danger',],
                ] );
            }
        }

        $families = $this->Plants->Families->find('list', [
            'keyField' => 'id',
            'valueField' => function ($result) {
                return $result->get('label');
            },
            'limit' => 200,
            'order' => ['name' => 'asc'],
        ]);
        $genera = $this->Plants->Genera->find('list', [
            'keyField' => 'id',
            'valueField' => function ($result) {
                return $result->get('label');
            },
            'limit' => 200,
            'order' => ['name' => 'asc'],
        ]);

        $items = [
            [
                'label' => 'Botanischer Name',
                'field' => 'name',
                'type'  => 'text',
            ],
            [
                'label' => 'Trivialname',
                'field' => 'trivialname',
                'type'  => 'text',
            ],
            [
                'label' => 'Image',
                'field' => 'image_file',
                'type'  => 'file',
            ],
            [
                'label'      => 'Family',
                'field'      => 'family_id',
                'type'       => 'select',
                'selectlist' => $families,
            ],
            [
                'label'      => 'Genus',
                'field'      => 'genus_id',
                'type'       => 'select',
                'selectlist' => $genera,
            ],
            [
                'label'      => 'Indoor?',
                'field'      => 'indoor',
                'type'       => 'checkbox',
            ],
        ];

        $sidenavitems = [
            [
                'name' => 'List all Plants',
                'action' => 'index',
            ],
        ];
        $this->set(compact('sidenavitems','plant', 'items'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Plant id.
     *
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = NULL)
    {
        $plant = $this->Plants->get($id);
        
        $this->Authorization->authorize($plant);

        if ( $this->request->is(['patch', 'post', 'put']) ) {

            $plant = $this->Plants->patchEntity($plant, $this->request->getData());

            if ( !$plant->getErrors() ) {

                $path = WWW_ROOT . 'img' . DS . 'plants';
                $image = $this->request->getData('image_change');
//              debug($image);
//              exit;
                $filename = $image->getClientFilename();

                if ( strlen($filename) > 0 ) {

                    if ( !is_dir($path) ) {

                        mkdir($path, 0755);
                    }
                    $targetPath = $path . DS . $filename;
                    $image->moveTo($targetPath);

                    if ( isset($plant->image) ) {

                        $imagePath = $path . DS . $plant->image;

                        if ( file_exists($imagePath) ) {
                            unlink($imagePath);
                        }
                    }
                    $plant->image = $filename;
                }
            }

            if ( $this->Plants->save($plant) ) {

                $this->Flash->info( __('The plant has been saved.'), [
                    'clear'  => TRUE,
                    'params' => ['typ' => 'success',],
                ] );
                return $this->redirect([
                    'action' => 'edit',
                    $id,
                ]);
            }
            $this->Flash->info( __('The plant could not be saved. Please, try again.'), [
                'clear'  => TRUE,
                'params' => ['typ' => 'warning',],
            ] );
        }

        $families = $this->Plants->Families->find('list', [
            'keyField' => 'id',
            'valueField' => function ($result) {
                return $result->get('label');
            },
            'limit' => 200,
            'order' => ['name' => 'asc'],
        ]);
        $genera = $this->Plants->Genera->find('list', [
            'keyField' => 'id',
            'valueField' => function ($result) {
                return $result->get('label');
            },
            'limit' => 200,
            'order' => ['name' => 'asc'],
        ]);

        $items = [
            [
                'label' => 'Botanischer Name',
                'field' => 'name',
                'type'  => 'text',
            ],
            [
                'label' => 'Trivialname',
                'field' => 'trivialname',
                'type'  => 'text',
            ],
            [
                'label' => 'Upload Image',
                'field' => 'image_change',
                'type'  => 'file',
            ],
            [
                'label' => 'Indoor?',
                'field' => 'indoor',
                'type'  => 'checkbox',
            ],
            [
                'label' => 'Current Image',
                'field' => 'image_current',
                'text' => 'plants/' . $plant->image,
                'type'  => 'image',
                'alt' => $plant->name,
                'class' => 'img-thumbnail rounded float-start plantpic',
            ],
            [
                'label'      => 'Family',
                'field'      => 'family_id',
                'type'       => 'select',
                'selectlist' => $families,
            ],
            [
                'label'      => 'Genus',
                'field'      => 'genus_id',
                'type'       => 'select',
                'selectlist' => $genera,
            ],
            [
                'label' => 'Description',
                'field' => 'comment',
                'type'  => 'textarea',
            ],
        ];

        $sidenavitems = [
            [
                'name' => 'List all Plants',
                'action' => 'index',
            ],
            [
                'name' => 'Details',
                'action' => 'view',
                'param' => $id,
            ],
            [
                'action' => 'delete',
                'param' => $id,
                'postlink' => __('Are you sure you want to delete {0}?', $plant->name),
            ],
        ];

        $tblitems = [
            [
                'name' => 'Link',
                'field' => [
                    'name' => 'name',
                    'type' => 'externallink',
                    'linktext' => 'comment',
                ],
                'sort' => 'comment',
            ],
            [
                'name' => 'Actions',
                'class' => 'actions',
                'actionitems' => [
                    [
                        'controller' => 'Infolinks',
                        'action' => 'edit',
                        'param' => 'id',
                    ],
                    [
                        'controller' => 'Infolinks',
                        'action' => 'delete',
                        'param' => 'id',
                        'postlink' => __('Are you sure you want to delete?'),
                    ],
                ],
            ],
        ];
        $infolinks = $this->paginate($this->Infolinks->find()->where(['plant_id' => $id]), ['model' => 'Infolinks']);

        $this->set(compact('sidenavitems','plant', 'families', 'genera', 'items', 'infolinks', 'tblitems'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Plant id.
     *
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = NULL)
    {

        $this->request->allowMethod(['post', 'delete']);
        $plant = $this->Plants->get($id);

        $this->Authorization->authorize($plant);
        
        if ( $this->Plants->delete($plant) ) {
            $message = 'The plant has been deleted.';
            $params = ['typ' => 'success',];

        } else {
            $message = 'The plant could not be deleted. Please, try again.';
            $params = ['typ' => 'warning',];

        }
        $this->Flash->info( __($message), [
            'clear'  => TRUE,
            'params' => $params,
        ] );

        return $this->redirect(['action' => 'index']);
    }

}

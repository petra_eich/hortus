<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
    }
    
    
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Authentication->addUnauthenticatedActions(['login', 'register']);
    }


    public function login()
    {
        $this->Authorization->skipAuthorization();
        
        $this->request->allowMethod(['get', 'post']);
        $result = $this->Authentication->getResult();

        // regardless of POST or GET, redirect if user is logged in
        if ($result->isValid()) {
            $user = $this->Authentication->getIdentity();

            if ($user->status == 0) {

                return $this->redirect([
                    'controller' => 'Plants',
                    'action' => 'index',
                ]);
            } else {

                $this->Flash->error(__('You not have the access permission yet!'));
                return $this->redirect([
                    'controller' => 'Users',
                    'action' => 'logout',
                ]);
            }
        }
        $this->viewBuilder()->setLayout('userlayout');

        // display error if user submitted and authentication failed
        if ($this->request->is('post') && !$result->isValid()) {

            $this->Flash->error(__('Invalid username or password'));
        }
    }


    public function logout()
    {
        $this->Authorization->skipAuthorization();      
        $result = $this->Authentication->getResult();

        // regardless of POST or GET, redirect if user is logged in
        if ($result->isValid()) {

            $this->Authentication->logout();
            return $this->redirect([
                'controller' => 'Users',
                'action' => 'login',
            ]);
        }
    }

    /**
     * Register method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function register()
    {
        $this->Authorization->skipAuthorization();
        $user = $this->Users->newEmptyEntity();

        if ( $this->request->is('post') ) {

            $user = $this->Users->patchEntity($user, $this->request->getData());

            if ( $this->Users->save($user) ) {

                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect([
                    'controller' => 'Users',
                    'action' => 'login',
                ]);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));

        }
        $this->viewBuilder()->setLayout('userlayout');
        $this->set(compact('user'));
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'order' => [
                'username' => 'asc',
            ],
        ];
        $users = $this->paginate($this->Users);
        
        $user = $this->Users->find()->first();
        $this->Authorization->authorize($user);
        
        $sidenavitems = [
            ['action' => 'add'],
        ];
        
        $this->viewBuilder()->setLayout('mylayout');
        $this->set(compact('sidenavitems', 'users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        try {
            $user = $this->Users->get($id, [
                'contain' => [],
            ]);
        } catch (RecordNotFoundException $e) {
            
            $this->Flash->info(__('User do not exist.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
            return $this->redirect(['action' => 'index']);
        }
        
        $this->Authorization->authorize($user);
        
        $sidenavitems = [
            [ 'action' => 'index'],
        ];
        
        $this->viewBuilder()->setLayout('mylayout');
        $this->set(compact('sidenavitems', 'user'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {       
        $user = $this->Users->newEmptyEntity();

        $this->Authorization->authorize($user);
        
        if ( $this->request->is('post') ) {

            $user = $this->Users->patchEntity($user, $this->request->getData());

            if ( $this->Users->save($user) ) {

                $this->Flash->info(__('The user has been saved.'), [
                    'clear' => true,
                    'params' => ['typ' => 'success',],
                ]);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->info(__('The user could not be saved. Please, try again.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);

        }
        $sidenavitems = [
            [ 'action' => 'index'],
        ];
        
        $this->viewBuilder()->setLayout('mylayout');
        $this->set(compact('sidenavitems', 'user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        
        $this->Authorization->authorize($user);
        
        if ( $this->request->is(['patch', 'post', 'put']) ) {

            $user = $this->Users->patchEntity($user, $this->request->getData());

            if ( $this->Users->save($user) ) {

                $this->Flash->info(__('The user has been saved.'), [
                    'clear' => true,
                    'params' => ['typ' => 'success',],
                ]);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->info(__('The user could not be saved. Please, try again.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
        }
        $sidenavitems = [
            ['action' => 'index'],
            [
                'action' => 'delete',
                'param' => $id,
                'postlink' => __('Are you sure you want to delete {0}?', $user->username),
            ]
        ];

        $this->viewBuilder()->setLayout('mylayout');
        $this->set(compact('sidenavitems','user'));
    }


//    public function userStatus($id = null, $status)
//    {
//        $this->request->allowMethod(['post']);
//        $user = $this->Users->get($id);
//
//        $user->status = ($status == 1) ? 0 : 1;
////        $this->Users->save($user);
//
//        if ( $this->Users->save($user) ) {
//
//            $this->Flash->success(__('User status has changed.'));
//        } else {
//            
//            $this->Flash->error(__('User status has not changed. Please, try again.'));
//        }
//        return $this->redirect(['action' => 'index']);
////        debug($user);
////        exit();
//    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);

        $this->Authorization->authorize($user);
        
        if ( $this->Users->delete($user) ) {
            
            $message = 'The user has been deleted.';
            $params = ['typ' => 'success',];
        } else {
            
            $message = 'The user could not be deleted. Please, try again.';
            $params = ['typ' => 'warning',];

        }
        $this->Flash->info(__($message), [
            'clear' => true,
            'params' => $params,
        ]);
        return $this->redirect(['action' => 'index']);
    }
}

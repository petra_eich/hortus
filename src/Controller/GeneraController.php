<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * Genera Controller
 *
 * @property \App\Model\Table\GeneraTable $Genera
 * @method \App\Model\Entity\Genus[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GeneraController extends AppController
{

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->Authorization->skipAuthorization();
        
        $key = $this->request->getQuery('key');
        
        if ( $key ) {
            
            $query = $this->Genera->find('all')->where(['OR' => [
                    ['Genera.name LIKE' => '%' . $key. '%'],
                    ['Genera.trivialname LIKE' => '%' . $key. '%'],
                    ['Families.name LIKE' => '%' . $key. '%'],
                    ['Families.trivialname LIKE' => '%' . $key. '%'],
                ],
            ]);
        } else {
            
            $query = $this->Genera;
        }
        
        $this->paginate = [
            'contain' => [
                'Families',
                'Plants',
            ],
            'order' => [
                'name' => 'asc',
            ],
        ];
        $genera = $this->paginate($query, [
            'sortableFields' => [
                'name', 
                'trivialname', 
                'Families.name'
            ]
        ]);

        $tblitems = [
            [
                'name' => 'Botanischer Name',
                'field' => [
                    'name' => 'name',
                ],
                'sort' => 'name',
            ],
            [
                'name' => 'Trivailname',
                'field' => [
                    'name' => 'trivialname',
                ],
                'sort' => 'trivialname',
            ],
            [
                'name' => 'Family',
                'field' => [
                    'name' => 'family_id',
                    'condition' => 'family',
                    'type' => 'link',
                    'link' => [
                        'controller' => 'Families',
                        'action' => 'view',
                        'linktext' => 'name',
                        'zusatz' => 'trivialname',
                    ]
                ],
                'sort' => 'Families.name',
            ],
            [
                'name' => 'Actions',
                'class' => 'actions',
                'actionitems' => [
                    [
                        'action' => 'edit',
                        'param' => 'id',
                    ],
                    [
                        'action' => 'view',
                        'param' => 'id',
                    ],
                    [
                        'condition' => ['plants', 'family'],
                        'action' => 'delete',
                        'param' => 'id',
                        'postlink' => __('Are you sure you want to delete?'),
                    ],
                ],
            ],
        ];

        $sidenavitems = [
            ['action' => 'add'],
        ];
        $this->set(compact('sidenavitems','genera', 'tblitems'));
    }

    /**
     * View method
     *
     * @param string|null $id Genus id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->Authorization->skipAuthorization();
                       
        try {
            
            $genus = $this->Genera->get($id, [
                'contain' => ['Families',],
            ]);
        } catch (RecordNotFoundException $e) {
            
            $this->Flash->info(__('Plant Genus do not exist.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
            return $this->redirect(['action' => 'index']);
        }
        
        $items = [
            [
                'label' => 'Botanischer Name',
                'text' => $genus->name,
                'type'  => 'text',
            ],
            [
                'label' => 'Trivialname',
                'text' => $genus->trivialname,
                'type'  => 'text',
            ],
            [
                'label'      => 'Family',
                'type'       => 'text',
                'text'       => $genus->family->name,
                'link'      => ['controller' => 'Families', 'action' => 'view', $genus->family_id]
            ],
            [
                'label' => 'Bemerkung',
                'field' => $genus->comment,
                'type'  => 'textarea',
            ],
        ];

        $query = $this->Genera->Plants
            ->find()
            ->where(['genus_id' => $id]);
        $plants = $this->paginate($query, ['sortableFields' => ['name', 'trivialname',]]);

        $tblitems = [];

        if ( count($plants) > 0 ) {

            $tblitems = [
                [
                    'name' => 'Botanischer Name',
                    'field' => [
                        'name' => 'name',
                    ],
                    'sort' => 'name',
                ],
                [
                    'name' => 'Trivialname',
                    'field' => [
                        'name' => 'trivialname',
                    ],
                    'sort' => 'trivialname',
                ],
                [
                    'name' => 'Image',
                    'field' => [
                        'name' => 'image',
                        'type' => 'imagelink',
                        'alt' => 'name',
                        'class' => 'img-thumbnail rounded float-start tblpic',
                        'dir' => 'plants',
                        'link' => [
                            'controller' => 'Plants',
                            'action' => 'view',
                            'param' => 'id',
                        ]
                    ],
                ],
            ];
        }

        $sidenavitems = [
            ['action' => 'index',],
            [
                'action' => 'edit',
                'param' => $id,
            ],
        ];

        $this->set(compact('sidenavitems','genus', 'items', 'plants', 'tblitems'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $genus = $this->Genera->newEmptyEntity();
        
        $this->Authorization->authorize($genus);

        if ($this->request->is('post')) {

            $genus = $this->Genera->patchEntity($genus, $this->request->getData());

            if ($this->Genera->save($genus)) {

                $this->Flash->info(__('The Plant Genus has been saved.'), [
                    'clear' => true,
                    'params' => ['typ' => 'success',],
                ]);
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->info(__('The Plant Genus could not be saved. Please, try again.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
        }

        $families = $this->Genera->Families->find('list', [
            'keyField' => 'id',
            'valueField' => function ($result) {
                return $result->get('label');
            },
            'limit' => 200,
            'order' => ['name' => 'asc'],
        ]);

        $items = [
            [
                'label' => 'Botanischer Name',
                'field' => 'name',
                'type'  => 'text',
            ],
            [
                'label' => 'Trivialname',
                'field' => 'trivialname',
                'type'  => 'text',
            ],
            [
                'label'      => 'Family',
                'field'      => 'family_id',
                'type'       => 'select',
                'selectlist' => $families,
            ],
            [
                'label' => 'Bemerkung',
                'field' => 'comment',
                'type'  => 'textarea',
            ],
        ];

        $sidenavitems = [
            [
                'name' => 'List all Plant Gernera',
                'action' => 'index',
            ],
        ];
        $this->set(compact('sidenavitems','genus', 'items'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Genus id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {            
        $genus = $this->Genera->get($id, [
            'contain' => [
                'Families',
                'Plants',
            ],
        ]);
        
        if ( !$genus ) {
            
            $this->Flash->info(__('Plant Genus do not exist.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
            return $this->redirect(['action' => 'index']);
        } else {
            
            $this->Authorization->authorize($genus);
         }


        if ($this->request->is(['patch', 'post', 'put'])) {

            $genus = $this->Genera->patchEntity($genus, $this->request->getData());

            if ($this->Genera->save($genus)) {
                $this->Flash->info(__('The Plant Genus {0} has been saved.', $genus->name), [
                    'clear' => true,
                    'params' => ['typ' => 'success',],
                ]);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->info(__('The Plant Genus {0} could not be saved. Please, try again.', $genus->name), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
        }
        $sidenavitems = [
            ['action' => 'index'],
            [
                'action' => 'view',
                'param' => $id,
            ],
        ];

        if ( empty($genus->family) && empty($genus->plants) ) {

            $sidenavitems[] = [
                'action' => 'delete',
                'param' => $id,
                'postlink' => __('Are you sure you want to delete {0}?', $genus->name),
            ];
        }

        $families = $this->Genera->Families->find('list', [
            'keyField' => 'id',
            'valueField' => function ($result) {
                return $result->get('label');
            },
            'limit' => 200,
            'order' => ['name' => 'asc'],
        ]);

        $items = [
            [
                'label' => 'Botanischer Name',
                'field' => 'name',
                'type'  => 'text',
            ],
            [
                'label' => 'Trivialname',
                'field' => 'trivialname',
                'type'  => 'text',
            ],
            [
                'label'      => 'Family',
                'field'      => 'family_id',
                'type'       => 'select',
                'selectlist' => $families,
            ],
            [
                'label' => 'Bemerkung',
                'field' => 'comment',
                'type'  => 'textarea',
            ],
        ];

        $this->set(compact ('sidenavitems', 'genus', 'items'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Genus id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $genus = $this->Genera->get($id);
        
        $this->Authorization->authorize($genus);
        
        if ($this->Genera->delete($genus)) {
            
            $message = 'The Plant Genus has been deleted.';
            $params = ['typ' => 'success',];
        } else {
            
            $message = 'The Plant Genus could not be deleted. Please, try again.';
            $params = ['typ' => 'warning',];

        }
        $this->Flash->info(__($message), [
            'clear' => true,
            'params' => $params,
        ]);
        return $this->redirect(['action' => 'index']);
    }
}

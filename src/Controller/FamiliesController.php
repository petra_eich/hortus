<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * Families Controller
 *
 * @property \App\Model\Table\FamiliesTable $Families
 * @method \App\Model\Entity\Family[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FamiliesController extends AppController
{

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->loadModel('Plants');
        $this->loadModel('Genera');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->Authorization->skipAuthorization();
        
        $key = $this->request->getQuery('key');
        
        if ( $key ) {
            
            $query = $this->Families->find('all')->where(['OR' => [
                ['name LIKE' => '%' . $key. '%'],
                ['trivialname LIKE' => '%' . $key. '%'],
                ],
            ]);
        } else {
            
            $query = $this->Families;
        }
        
        $this->paginate = [
            'contain' => [
                'Plants',
            ],
            'order' => [
                'name' => 'asc',
            ],
        ];
        $families = $this->paginate($query, [
            'sortableFields' => [
                'name', 
                'trivialname',                        
            ]
        ]);
        
        $tblitems = [
            [
                'name' => 'Botanischer Name',
                'field' => [
                    'name' => 'name',
                ],
                'sort' => 'name',
            ],
            [
                'name' => 'Trivailname',
                'field' => [
                    'name' => 'trivialname',
                ],
                'sort' => 'trivialname',
            ],
            [
                'name' => 'Actions',
                'class' => 'actions',
                'actionitems' => [
                    [
                        'action' => 'edit',
                        'param' => 'id',
                    ],
                    [
                        'action' => 'view',
                        'param' => 'id',
                    ],
                    [
                        'condition' => 'plants',
                        'action' => 'delete',
                        'param' => 'id',
                        'postlink' => __('Are you sure you want to delete?'),
                    ],
                ],
            ],
        ];

        $sidenavitems = [
            ['action' => 'add'],
        ];
        $this->set(compact('sidenavitems','families', 'tblitems'));
    }

    /**
     * View method
     *
     * @param string|null $id Family id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->Authorization->skipAuthorization();
        
        $this->paginate = [
            'order' => ['name' => 'asc',],
        ];
        
        try {
 
            $family = $this->Families->get($id);
        } catch (RecordNotFoundException $e) {
            
            $this->Flash->info(__('Family do not exist.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
            return $this->redirect(['action' => 'index']);
        }
                
        $items = [
            [
                'label' => 'Trivialname',
                'text' => $family->trivialname,
                'type'  => 'text',
            ],
            [
                'label' => 'Description',
                'text' => $family->comment,
                'type'  => 'textarea',
            ],
        ];

        $query = $this->Plants
            ->find()
            ->contain(['Genera'])
            ->where(['Plants.family_id' => $id]);
        $plants = $this->paginate($query, [
                'sortableFields' => ['name', 'trivialname', 'Genera.name'],
                'scope' => 'plant',
                'model' => 'Plants',
            ]);

        $tblitems = [];

        if ( !empty($plants) ) {

            $tblitems = [
                [
                    'name' => 'Botanischer Name',
                    'field' => [
                        'name' => 'name',
                    ],
                    'sort' => 'name',
                ],
                [
                    'name' => 'Trivialname',
                    'field' => [
                        'name' => 'trivialname',
                    ],
                    'sort' => 'trivialname',
                ],
                [
                    'name' => 'Image',
                    'field' => [
                        'name' => 'image',
                        'type' => 'imagelink',
                        'alt' => 'name',
                        'class' => 'img-thumbnail rounded float-start tblpic',
                        'dir' => 'plants',
                        'link' => [
                            'controller' => 'Plants',
                            'action' => 'view',
                            'param' => 'id',
                        ]
                    ],
                ],
                [
                    'name' => 'Plant Genus',
                    'field' => [
                        'name' => 'genus_id',
                        'condition' => 'genus',
                        'type' => 'link',
                        'link' => [
                            'controller' => 'Genera',
                            'action' => 'view',
                            'linktext' => 'name',
                            'zusatz' => 'trivialname',
                        ]
                    ],
                    'sort' => 'Genera.name',
                ],
            ];
        }

        $sidenavitems = [
            ['action' => 'index',],
            [
                'action' => 'edit',
                'param' => $id,
            ],
        ];

        $query = $this->Genera
            ->find()
            ->where(['family_id' => $id]);
        $genera = $this->paginate($query, [
                'sortableFields' => ['name', 'trivialname',],
                'scope' => 'genus',
                'model' => 'Gerna',
            ]);

        $gentblitems = [];

        if ( !empty($genera) ) {

            $gentblitems = [
                [
                    'name' => 'Botanischer Name',
                    'field' => [
                        'name' => 'name',
                    ],
                    'sort' => 'name',
                ],
                [
                    'name' => 'Trivialname',
                    'field' => [
                        'name' => 'trivialname',
                    ],
                    'sort' => 'trivialname',
                ],
                [
                    'name' => 'Actions',
                    'class' => 'actions',
                    'actionitems' => [
                        [
                            'controller' => 'Genera',
                            'action' => 'view',
                            'param' => 'id',
                        ],
                    ],
                ],
            ];
        }

        if ( empty($plants) || empty($genera) ) {

            $sidenavitems[] = [
                'action' => 'delete',
                'param' => $id,
                'postlink' => __('Are you sure you want to delete {0}?', $family->name),
            ];
        }

        $this->set(compact('sidenavitems','family', 'plants', 'items', 'tblitems', 'genera', 'gentblitems'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $family = $this->Families->newEmptyEntity();
        
        $this->Authorization->authorize($family);

        if ($this->request->is('post')) {

            $family = $this->Families->patchEntity($family, $this->request->getData());

            if ($this->Families->save($family)) {

                $this->Flash->info(__('The family has been saved.'), [
                    'clear' => true,
                    'params' => ['typ' => 'success',],
                ]);
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->info(__('The family could not be saved. Please, try again.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
        }

        $items = [
            [
                'label' => 'Botanischer Name',
                'field' => 'name',
                'type'  => 'text',
            ],
            [
                'label' => 'Trivialname',
                'field' => 'trivialname',
                'type'  => 'text',
            ],
            [
                'label' => 'Bemerkung',
                'field' => 'comment',
                'type'  => 'textarea',
            ],
        ];

        $sidenavitems = [
            ['action' => 'index'],
        ];
        $this->set(compact('sidenavitems','family', 'items'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Family id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $family = $this->Families->get($id, [
            'contain' => [
                'Plants',
            ],
        ]);

        $this->Authorization->authorize($family);
        
        if ($this->request->is(['patch', 'post', 'put'])) {

            $family = $this->Families->patchEntity($family, $this->request->getData());

            if ($this->Families->save($family)) {
                $this->Flash->info(__('The family {0} has been saved.', $family->name), [
                    'clear' => true,
                    'params' => ['typ' => 'success',],
                ]);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->info(__('The family {0} could not be saved. Please, try again.', $family->name), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
        }
        $sidenavitems = [
            ['action' => 'index'],
            [
                'action' => 'view',
                'param' => $id,
            ],
        ];

        if ( empty($family->plants) ) {

            $sidenavitems[] = [
                'action' => 'delete',
                'param' => $id,
                'postlink' => __('Are you sure you want to delete {0}?', $family->name),
            ];
        }

        $items = [
            [
                'label' => 'Botanischer Name',
                'field' => 'name',
                'type'  => 'text',
            ],
            [
                'label' => 'Trivialname',
                'field' => 'trivialname',
                'type'  => 'text',
            ],
            [
                'label' => 'Bemerkung',
                'field' => 'comment',
                'type'  => 'textarea',
            ],
        ];

        $this->set(compact ('sidenavitems', 'family', 'items'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Family id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $family = $this->Families->get($id);

        $this->Authorization->authorize($family);
        
        if ($this->Families->delete($family)) {
            
            $message = 'The family has been deleted.';
            $params = ['typ' => 'success',];
        } else {
            
            $message = 'The family could not be deleted. Please, try again.';
            $params = ['typ' => 'warning',];

        }
        $this->Flash->info(__($message), [
            'clear' => true,
            'params' => $params,
        ]);
        return $this->redirect(['action' => 'index']);
    }
}

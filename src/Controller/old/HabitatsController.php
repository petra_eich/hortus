<?php
declare(strict_types=1);

namespace App\Controller\old;

use App\Controller\AppController;
use function __;

/**
 * Habitats Controller
 *
 * @property \App\Model\Table\old\HabitatsTable $Habitats
 * @method \App\Model\Entity\old\Habitat[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HabitatsController extends AppController
{

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);

    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'Plants',
            ],
            'order' => [
                'name' => 'asc',
            ],
        ];
        $habitats = $this->paginate($this->Habitats);

        $tblitems = [
            [
                'name' => 'Name',
                'field' => [
                    'name' => 'name',
                ],
                'sort' => 'name',
            ],
            [
                'name' => 'Actions',
                'class' => 'actions',
                'actionitems' => [
                    [
                        'action' => 'edit',
                        'param' => 'id',
                    ],
                    [
                        'action' => 'view',
                        'param' => 'id',
                    ],
                    [
                        'condition' => 'plants',
                        'action' => 'delete',
                        'param' => 'id',
                        'postlink' => __('Are you sure you want to delete?'),
                    ],
                ],
            ],
        ];

        $sidenavitems = [
            ['action' => 'add'],
        ];
        $this->set(compact('sidenavitems','habitats', 'tblitems'));
    }

    /**
     * View method
     *
     * @param string|null $id Habitat id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $habitat = $this->Habitats->get($id, [
            'contain' => ['Plants'],
        ]);
        $items = [
            [
                'label' => 'Name',
                'text' => $habitat->name,
                'type'  => 'text',
            ],
            [
                'label' => 'Description',
                'field' => $habitat->description,
                'type'  => 'textarea',
            ],
        ];

        $query = $this->Habitats->Plants
            ->find()
            ->where(['habitat_id' => $id]);
        $plants = $this->paginate($query, ['sortWhitelist' => ['name', 'trivialname',]]);

        $tblitems = [
            [
                'name' => 'Botanischer Name',
                'field' => [
                    'name' => 'name',
                ],
                'sort' => 'name',
            ],
            [
                'name' => 'Trivialname',
                'field' => [
                    'name' => 'trivialname',
                ],
                'sort' => 'trivialname',
            ],
            [
                'name' => 'Actions',
                'class' => 'actions',
                'actionitems' => [
                    [
                        'controller' => 'Plants',
                        'action' => 'view',
                        'param' => 'id',
                    ],
                ],
            ],
        ];

        $sidenavitems = [
            ['action' => 'index',],
            [
                'action' => 'edit',
                'param' => $id,
            ],
        ];

        if ( empty($habitat->plants) ) {

            $sidenavitems[] = [
                'action' => 'delete',
                'param' => $id,
                'postlink' => __('Are you sure you want to delete {0}?', $habitat->name),
            ];
        }
        $this->set(compact('sidenavitems','habitat', 'plants', 'items', 'tblitems'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $habitat = $this->Habitats->newEmptyEntity();

        if ($this->request->is('post')) {

            $family = $this->Habitats->patchEntity($habitat, $this->request->getData());

            if ($this->Habitats->save($habitat)) {

                $this->Flash->info(__('The habitat has been saved.'), [
                    'clear' => true,
                    'params' => ['typ' => 'success',],
                ]);
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->info(__('The habitat could not be saved. Please, try again.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
        }

        $items = [
            [
                'label' => 'Name',
                'field' => 'name',
                'type'  => 'text',
            ],
            [
                'label' => 'Description',
                'field' => 'description',
                'type'  => 'textarea',
            ],
        ];

        $sidenavitems = [
            ['action' => 'index'],
        ];
        $this->set(compact('sidenavitems','habitat', 'items'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Habitat id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $habitat = $this->Habitats->get($id, [
            'contain' => [
                'Plants',
            ],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $range = $this->Habitats->patchEntity($habitat, $this->request->getData());

            if ($this->Habitats->save($habitat)) {

                $this->Flash->info(__('The habitat {0} has been saved.', $habitat->name), [
                    'clear' => true,
                    'params' => ['typ' => 'success',],
                ]);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->info(__('The habitat {0} could not be saved. Please, try again.', $habitat->name), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
        }
        $sidenavitems = [
            ['action' => 'index'],
            [
                'action' => 'view',
                'param' => $id,
            ],
        ];

        if ( empty($habitat->plants) ) {

            $sidenavitems[] = [
                'action' => 'delete',
                'param' => $id,
                'postlink' => __('Are you sure you want to delete {0}?', $habitat->name),
            ];
        }

        $items = [
            [
                'label' => 'Name',
                'field' => 'name',
                'type'  => 'text',
            ],
            [
                'label' => 'Description',
                'field' => 'description',
                'type'  => 'textarea',
            ],
        ];

        $this->set(compact ('sidenavitems', 'habitat', 'items'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Habitat id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $habitat = $this->Habitats->get($id);

        if ( $this->Ranges->delete($habitat) ) {

            $message = 'The habitat has been deleted.';
            $params = ['typ' => 'success',];
        } else {
            $message = 'The habitat could not be deleted. Please, try again.';
            $params = ['typ' => 'warning',];

        }
        $this->Flash->info(__($message), [
            'clear' => true,
            'params' => $params,
        ]);
        return $this->redirect(['action' => 'index']);
    }
}

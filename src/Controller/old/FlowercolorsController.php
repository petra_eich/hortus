<?php
declare(strict_types=1);

namespace App\Controller\old;

use App\Controller\AppController;
use function __;

/**
 * Flowercolors Controller
 *
 * @property \App\Model\Table\old\FlowercolorsTable $Flowercolors
 * @method \App\Model\Entity\old\Flowercolor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FlowercolorsController extends AppController
{

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);

    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'Plants',
            ],
            'order' => [
                'name' => 'asc',
            ],
        ];
        $flowercolors = $this->paginate($this->Flowercolors);

        $tblitems = [
            [
                'name' => 'Name',
                'field' => [
                    'name' => 'name',
                ],
                'sort' => 'name',
            ],
            [
                'name' => 'Actions',
                'class' => 'actions',
                'actionitems' => [
                    [
                        'action' => 'edit',
                        'param' => 'id',
                    ],
                    [
                        'action' => 'view',
                        'param' => 'id',
                    ],
                    [
                        'condition' => 'plants',
                        'action' => 'delete',
                        'param' => 'id',
                        'postlink' => __('Are you sure you want to delete?'),
                    ],
                ],
            ],
        ];

        $sidenavitems = [
            ['action' => 'add'],
        ];
        $this->set(compact('sidenavitems','flowercolors', 'tblitems'));
    }

    /**
     * View method
     *
     * @param string|null $id Flowercolor id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $flowercolor = $this->Flowercolors->get($id, [
            'contain' => ['Plants'],
        ]);

        $items = [];

        $query = $this->Flowercolors->Plants
            ->find()
            ->where(['flowercolor_id' => $id]);
        $plants = $this->paginate($query, ['sortWhitelist' => ['name', 'trivialname',]]);

        $tblitems = [
            [
                'name' => 'Name',
                'field' => [
                    'name' => 'name',
                ],
                'sort' => 'name',
            ],
            [
                'name' => 'Trivialname',
                'field' => [
                    'name' => 'trivialname',
                ],
                'sort' => 'trivialname',
            ],
            [
                'name' => 'Actions',
                'class' => 'actions',
                'actionitems' => [
                    [
                        'controller' => 'Plants',
                        'action' => 'view',
                        'param' => 'id',
                    ],
                ],
            ],
        ];

        $sidenavitems = [
            ['action' => 'index',],
            [
                'action' => 'edit',
                'param' => $id,
            ],
        ];

        if ( empty($flowercolor->plants) ) {

            $sidenavitems[] = [
                'action' => 'delete',
                'param' => $id,
                'postlink' => __('Are you sure you want to delete {0}?', $flowercolor->name),
            ];
        }
        $this->set(compact('sidenavitems','flowercolor', 'items', 'plants', 'tblitems'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $flowercolor = $this->Flowercolors->newEmptyEntity();

        if ($this->request->is('post')) {

            $flowercolor = $this->Flowercolors->patchEntity($flowercolor, $this->request->getData());

            if ($this->Flowercolors->save($flowercolor)) {

                $this->Flash->info(__('The flowercolor has been saved.'), [
                    'clear' => true,
                    'params' => ['typ' => 'success',],
                ]);
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->info(__('The flowercolor could not be saved. Please, try again.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
        }

        $items = [
            [
                'label' => 'Name',
                'field' => 'name',
                'type'  => 'text',
            ],
        ];

        $sidenavitems = [
            ['action' => 'index'],
        ];
        $this->set(compact('sidenavitems','flowercolor', 'items'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Flowercolor id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $flowercolor = $this->Flowercolors->get($id, [
            'contain' => [
                'Plants',
            ],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $flowercolor = $this->Flowercolors->patchEntity($flowercolor, $this->request->getData());

            if ($this->Flowercolors->save($flowercolor)) {
                $this->Flash->info(__('The Flower Petal Color {0} has been saved.', $flowercolor->name), [
                    'clear' => true,
                    'params' => ['typ' => 'success',],
                ]);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->info(__('The Flower Petal Color {0} could not be saved. Please, try again.', $flowercolor->name), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
        }
        $sidenavitems = [
            ['action' => 'index'],
            [
                'action' => 'view',
                'param' => $id,
            ],
        ];

        if ( empty($flowercolor->plants) ) {

            $sidenavitems[] = [
                'action' => 'delete',
                'param' => $id,
                'postlink' => __('Are you sure you want to delete {0}?', $flowercolor->name),
            ];
        }

        $items = [
            [
                'label' => 'Name',
                'field' => 'name',
                'type'  => 'text',
            ],
        ];

        $this->set(compact ('sidenavitems', 'flowercolor', 'items'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Customer id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $flowercolor = $this->Flowercolors->get($id);

        if ($this->Flowercolors->delete($flowercolor)) {
            $message = 'The Flower Petal Color has been deleted.';
            $params = ['typ' => 'success',];
        } else {
            $message = 'The Flower Petal Color could not be deleted. Please, try again.';
            $params = ['typ' => 'warning',];

        }
        $this->Flash->info(__($message), [
            'clear' => true,
            'params' => $params,
        ]);
        return $this->redirect(['action' => 'index']);
    }
}

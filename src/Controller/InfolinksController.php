<?php
declare(strict_types=1);

namespace App\Controller;

use phpDocumentor\Reflection\Types\Null_;
use Cake\Http\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * Infolinks Controller
 *
 * @property \App\Model\Table\InfolinksTable $Infolinks
 * @method \App\Model\Entity\Infolink[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InfolinksController extends AppController
{
    
    public function initialize(): void
    {
        parent::initialize();
    }

    public function beforeFilter( \Cake\Event\EventInterface $event )
    {
        parent::beforeFilter($event);
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index($plant_id = NULL)
    {
        $this->Authorization->skipAuthorization();
        
        $key = $this->request->getQuery('key');
//        exit($key);
        if ( $key ) {
            
            $query = $this->Infolinks->find('all')->where(['OR' => [
                    ['Plants.name LIKE' => '%' . $key. '%'],
                    ['Plants.trivialname LIKE' => '%' . $key. '%'],
                    ['Infolinks.name LIKE' => '%' . $key. '%'],
                    ['Infolinks.comment LIKE' => '%' . $key. '%'],
                ],
            ]);
        } else {
            
            $query = $this->Infolinks;
        }
        
        $this->paginate = [
            'contain' => ['Plants'],
            'fields' => [
                'id',
                'plant_id',
                'name',
                'comment',
                'Plants.name',
                'Plants.image',
            ],
        ];
        $toptitle = Null;
        $plant = NULL;

        $tblitems = [
            [
                'name' => 'Link',
                'field' => [
                    'name' => 'name',
                    'type' => 'externallink',
                    'linktext' => 'comment',
                ],
                'sort' => 'comment',
            ],
        ];

        $sidenavitems = [
            [
                'name' => 'List all Plants',
                'controller' => 'Plants',
                'action' => 'index',
            ],
        ];

        if ( isset($plant_id) ) {

            $plant = $this->Infolinks->Plants->get($plant_id);
            $this->paginate['conditions'] = ['plant_id' => $plant_id];

            $sidenavitems = [
                [
                    'action'     => 'view',
                    'name'       => 'Back to ' . $plant->name,
                    'controller' => 'Plants',
                    'param'      => $plant_id,
                    'btncolor'   => 'success',
                ],
            ];
            $sidenavitems[] = [
                'name'   => 'List all Links',
                'action' => 'index',
            ];
            $sort = [
                'sortableFields' => [
                    'name', 
                    'comment',                    
                ]
            ];     
            $toptitle = $plant->name;
        } else {

            $tblitems[] = [
                'name' => 'Plants',
                'field' => [
                    'name' => 'plant_id',
                    'condition' => 'plant',
                    'type' => 'link',
                    'link' => [
                        'controller' => 'Plants',
                        'action' => 'view',
                        'linktext' => 'name',
                    ]
                ],
                'sort' => 'Plants.name',
            ];
            $tblitems[] = [
                'name' => 'Image',
                'field' => [
                    'name' => 'image',
                    'type' => 'imagelink',
                    'alt' => 'name',
                    'class' => 'img-thumbnail rounded float-start tblpic',
                    'dir' => 'plants',
                    'condition' => 'plant',
                    'link' => [
                        'controller' => 'Plants',
                        'action' => 'view',
                        'param' => 'plant_id',
                    ]
                ],
            ];
            $sort = [
                'sortableFields' => [
                    'name', 
                    'comment',                    
                    'Plants.name', 
                ]
            ];
        }

        $tblitems1 = [
            [
                'name' => 'Actions',
                'class' => 'actions',
                'actionitems' => [
                    [
                        'action' => 'edit',
                        'param' => 'id',
                    ],
                    [
                        'condition' => 'plants',
                        'action' => 'delete',
                        'param' => 'id',
                        'postlink' => __('Are you sure you want to delete?'),
                    ],
                ],
            ],
        ];
        $tblitems = array_merge($tblitems, $tblitems1);

        $infolinks = $this->paginate($query, $sort);

        $this->set(compact('sidenavitems', 'plant', 'infolinks', 'tblitems', 'toptitle'));
    }

    /**
     * Add method
     *
     * @param string|null $plant_id Plant id.
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add($plant_id = null)
    {
        $infolink = $this->Infolinks->newEmptyEntity();

        $this->Authorization->authorize($infolink);
        
        if ( $this->request->is('post') ) {

            $infolink = $this->Infolinks->patchEntity($infolink, $this->request->getData());

            if ( $infolink->getErrors()) {

                $this->Flash->info(__('The link could not be saved. Please, try again.'), [
                    'clear' => true,
                    'params' => ['typ' => 'danger',],
                ]);
            } else {

                if ($this->Infolinks->save($infolink)) {

                    $this->Flash->info( __( 'The link has been saved.' ), [
                        'clear'  => TRUE,
                        'params' => ['typ' => 'success',],
                    ] );

                    return $this->redirect( [
                        'controller' => 'Plants',
                        'action'     => 'edit',
                        $plant_id
                    ] );
                } else {

                    $this->Flash->info(__('The blink could not be saved. Please, try again.'), [
                        'clear' => true,
                        'params' => ['typ' => 'danger',],
                    ]);
                }
            }
        }
        $plant = $this->Infolinks->Plants->get($plant_id);

        $items = [
            [
                'label' => 'Link',
                'field' => 'name',
                'type'  => 'text',
            ],
            [
                'field' => 'plant_id',
                'type'  => 'hidden',
                'val'  => $plant_id,
            ],
            [
                'label' => 'Description',
                'field' => 'comment',
                'type'  => 'textarea',
            ],
        ];

        $sidenavitems = [
            [
                'name' => 'Back to ' . $plant->name,
                'controller' => 'Plants',
                'action' => 'edit',
                'param' => $plant_id,
                'btncolor' => 'success',
            ],
            [
                'action' => 'index',
                'param' => $plant_id,
            ],
        ];

        $this->set(compact('sidenavitems', 'infolink', 'items'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Infolink id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $infolink = $this->Infolinks->get($id, [
            'contain' => ['Plants'],
        ]);

        $this->Authorization->authorize($infolink);
        
        if ( $this->request->is(['patch', 'post', 'put']) ) {

            $infolink = $this->Infolinks->patchEntity($infolink, $this->request->getData());

            if ( $this->Infolinks->save($infolink) ) {

                $this->Flash->info(__('The link has been saved.'), [
                    'clear' => true,
                    'params' => ['typ' => 'success',],
                ]);

                return $this->redirect([
                    'controller' => 'projects',
                    'action' => 'edit',
                    $infolink->plant->id,
                ]);
            }
            $this->Flash->info(__('The link could not be saved. Please, try again.'), [
                'clear' => true,
                'params' => ['typ' => 'danger',],
            ]);
        }

        $items = [
            [
                'label' => 'Link',
                'field' => 'name',
                'type'  => 'text',
            ],
            [
                'label' => 'Description',
                'field' => 'comment',
                'type'  => 'text',
            ],
        ];

        $sidenavitems = [
            [
                'name' => 'Back to ' . $infolink->plant->name,
                'controller' => 'Plants',
                'action' => 'edit',
                'param' => $infolink->plant_id,
                'btncolor' => 'success',
            ],
            [
                'action' => 'index',
                'param' => $infolink->plant_id,
            ],
        ];
        $this->set(compact('sidenavitems','infolink', 'items'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Infolinks id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $infolink = $this->Infolinks->get($id);
        $plant_id = $infolink->plant_id;

        $this->Authorization->authorize($infolink);
        
        if ( $this->Infolinks->delete($infolink) ) {

            $message = 'The link has been deleted.';
            $params = ['typ' => 'success',];
        } else {

            $message = 'The link could not be delete. Please, try again.';
            $params = ['typ' => 'danger',];
        }

        $this->Flash->info(__($message), [
            'clear' => true,
            'params' => $params,
        ]);

        return $this->redirect([
            'controller' => 'Plants',
            'action' => 'edit',
            $plant_id,
        ]);
    }
}

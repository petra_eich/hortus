<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Infolink;
use Authorization\IdentityInterface;

/**
 * Infolink policy
 */
class InfolinkPolicy
{
    /**
     * Check if $user can add Infolink
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Infolink $infolink
     * @return bool
     */
    public function canAdd(IdentityInterface $user, Infolink $infolink)
    {
        // All logged in users can create
        return true;
    }

    /**
     * Check if $user can edit Infolink
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Infolink $infolink
     * @return bool
     */
    public function canEdit(IdentityInterface $user, Infolink $infolink)
    {
        return $this->isAdmin($user);
    }

    /**
     * Check if $user can delete Infolink
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Infolink $infolink
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Infolink $infolink)  
    {
        return $this->isAdmin($user);
    }

    /**
     * Check if $user has role admin
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @return bool
     */
    protected function isAdmin(IdentityInterface $user)
    {
        return $user->get('role') === 'admin';
    }
    
}

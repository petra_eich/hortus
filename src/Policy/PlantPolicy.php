<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Plant;
use Authorization\IdentityInterface;

/**
 * Plant policy
 */
class PlantPolicy
{
    /**
     * Check if $user can add Plant
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Plant $plant
     * @return bool
     */
    public function canAdd(IdentityInterface $user, Plant $plant)
    {
        // All logged in users can create 
        return true;
    }

    /**
     * Check if $user can edit Plant
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Plant $plant
     * @return bool
     */
    public function canEdit(IdentityInterface $user, Plant $plant)
    {
        return $this->isAdmin($user);
    }

    /**
     * Check if $user can delete Plant
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Plant $plant
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Plant $plant)
    {
        return $this->isAdmin($user);
    }

    /**
     * Check if $user has role admin
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @return bool
     */
    protected function isAdmin(IdentityInterface $user)
    {
        return $user->get('role') === 'admin';
    }
}

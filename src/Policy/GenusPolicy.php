<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Genus;
use Authorization\IdentityInterface;

/**
 * Genus policy
 */
class GenusPolicy
{
    /**
     * Check if $user can add Genus
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Genus $genus
     * @return bool
     */
    public function canAdd(IdentityInterface $user, Genus $genus)
    {
        // All logged in users can create
        return true;
    }

    /**
     * Check if $user can edit Genus
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Genus $genus
     * @return bool
     */
    public function canEdit(IdentityInterface $user, Genus $genus)
    {
        return $this->isAdmin($user);
    }

    /**
     * Check if $user can delete Genus
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Genus $genus
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Genus $genus)
    {
        return $this->isAdmin($user);
    }

    /**
     * Check if $user has role admin
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @return bool
     */
    protected function isAdmin(IdentityInterface $user)
    {
        return $user->get('role') === 'admin';
    }
}

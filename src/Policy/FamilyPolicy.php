<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Family;
use Authorization\IdentityInterface;

/**
 * Family policy
 */
class FamilyPolicy
{
    /**
     * Check if $user can add Family
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Family $family
     * @return bool
     */
    public function canAdd(IdentityInterface $user, Family $family)
    {
        // All logged in users can create 
        return true;
    }

    /**
     * Check if $user can edit Family
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Family $family
     * @return bool
     */
    public function canEdit(IdentityInterface $user, Family $family)
    {
        return $this->isAdmin($user);
    }

    /**
     * Check if $user can delete Family
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Family $family
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Family $family)
    {
        return $this->isAdmin($user);
    }

    /**
     * Check if $user has role admin
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @return bool
     */    
    protected function isAdmin(IdentityInterface $user)
    {
        return $user->get('role') === 'admin';
    }
}

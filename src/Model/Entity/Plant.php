<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Plant Entity
 *
 * @property int $id
 * @property int $family_id
 * @property int $genus_id
 * @property string $name
 * @property string|null $trivialname
 * @property string|null $image
 * @property bool $indoor
 * @property string|null $comment
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 */
class Plant extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'family_id' => TRUE,
        'genus_id' => TRUE,
        'name' => TRUE,
        'trivialname' => TRUE,
        'image' => TRUE,
        'comment' => TRUE,
        'indoor' => TRUE,
        'created' => TRUE,
        'modified' => TRUE,

        'infolinks' => TRUE, //has many

        'genus' => TRUE, //belongs to
        'habitat' => TRUE, //belongs to
    ];

    protected function _getLabel()
    {
        $trivial = strlen($this->_fields['trivialname']) > 0 ? ' / ' . $this->_fields['trivialname'] : '';
        return $this->_fields['name'] .  $trivial;
    }
}

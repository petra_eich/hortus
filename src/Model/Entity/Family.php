<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FamilyEntity
 *
 * @property int $id
 * @property string $name
 * @property string $trivialname
 * @property string|null $comment
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 */
class Family extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'trivialname' => true,
        'comment' => true,
        'created' => true,
        'modified' => true,

        'plants' => true, // has many
        'genera' => true, // has many
    ];

    protected function _getLabel()
    {
        $trivial = strlen($this->_fields['trivialname']) > 0 ? ' / ' . $this->_fields['trivialname'] : '';
        return $this->_fields['name'] .  $trivial;
    }
}

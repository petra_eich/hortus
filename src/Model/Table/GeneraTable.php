<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Genera Model
 *
 * @property \App\Model\Table\FamiliesTable&\Cake\ORM\Association\BelongsTo $Families
 *
 * @method \App\Model\Entity\Genus newEmptyEntity()
 * @method \App\Model\Entity\Genus newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Genus[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Genus get($primaryKey, $options = [])
 * @method \App\Model\Entity\Genus findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Genus patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Genus[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Genus|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Genus saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Genus[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Genus[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Genus[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Genus[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class GeneraTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('genera');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Plants', [
            'foreignKey' => 'genus_id',
        ]);

        $this->belongsTo('Families', [
            'foreignKey' => 'family_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator = new Validator();

        $validator
            // name
            ->notEmptyString('name','enter name')
            ->maxLength('name',100,'too long')
            ->minLength('name',2,'too short')
            ->requirePresence('name','create','enter name at all')
            ->add('name',
                'unique',
                [
                    'rule' => 'validateUnique',
                    'provider' => 'table','message' => 'duplicate entry',
                ]
            )
            // trivialname
            ->maxLength('trivialname',100,'too long')
            ->minLength('trivialname',2,'too short')
            ->allowEmptyString('trivialname')
            // comment
            ->scalar('comment')
            ->allowEmptyString('comment')
            // active
            ->boolean('indoor')
            ->notEmptyString('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['name']), ['errorField' => 'name']);
        $rules->add($rules->existsIn(['family_id'], 'Families'), ['errorField' => 'family_id']);

        return $rules;
    }
}

<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Plants Model
 *
 * @property \App\Model\Table\FamiliesTable&\Cake\ORM\Association\BelongsTo $Families
 * @property \App\Model\Table\GeneraTable&\Cake\ORM\Association\BelongsTo $Genera
 *
 * @method \App\Model\Entity\Plant newEmptyEntity()
 * @method \App\Model\Entity\Plant newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Plant[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Plant get($primaryKey, $options = [])
 * @method \App\Model\Entity\Plant findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Plant patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Plant[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Plant|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Plant saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Plant[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Plant[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Plant[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Plant[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PlantsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('plants');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Families', [
            'foreignKey' => 'family_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Genera', [
            'foreignKey' => 'genus_id',
            'joinType' => 'INNER',
        ]);

        $this->hasMany('Infolinks', [
            'foreignKey' => 'plant_id',
            'dependent' => TRUE,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator = new Validator();

        $validator
            // name
            ->notEmptyString('name','enter name')
            ->maxLength('name',100,'too long')
            ->minLength('name',2,'too short')
            ->requirePresence('name','create','enter name at all')
            ->add('name',
                'unique',
                [
                    'rule' => 'validateUnique',
                    'provider' => 'table','message' => 'duplicate entry',
                ]
            )
            // trivialname
            ->maxLength('trivialname',100,'too long')
            ->minLength('trivialname',2,'too short')
            ->allowEmptyString('trivialname')
            // image
            ->allowEmptyFile('image')
            ->add('image',[
                'mimeType' => [
                    'rule' => ['mimeType', ['image/jpg','image/png', 'image/jpeg']],
                    'message' => 'Pleace upload jpg or png.'
                ],
                'fileSize' => [
                    'rule' => ['fileSize', '<=','1MB'],
                    'message' => 'Size must be less than 1MB.'
                ],
            ])
            // comment
            ->scalar('comment')
            ->allowEmptyString('comment')
            // active
            ->boolean('indoor')
            ->notEmptyString('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['name']), ['errorField' => 'name']);

        $rules->add($rules->existsIn(['family_id'], 'Families'), ['errorField' => 'family_id']);
        $rules->add($rules->existsIn(['genus_id'], 'Genera'), ['errorField' => 'genus_id']);

        return $rules;
    }
}

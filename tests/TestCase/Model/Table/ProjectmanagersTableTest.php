<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProjectmanagersTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProjectmanagersTable Test Case
 */
class ProjectmanagersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ProjectmanagersTable
     */
    protected $Projectmanagers;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Projectmanagers',
        'app.Projects',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Projectmanagers') ? [] : ['className' => ProjectmanagersTable::class];
        $this->Projectmanagers = $this->getTableLocator()->get('Projectmanagers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Projectmanagers);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

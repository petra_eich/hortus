<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\old\MysqldbsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MysqldbsTable Test Case
 */
class MysqldbsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\old\MysqldbsTable
     */
    protected $Mysqldbs;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Mysqldbs',
        'app.Projects',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Mysqldbs') ? [] : ['className' => MysqldbsTable::class];
        $this->Mysqldbs = $this->getTableLocator()->get('Mysqldbs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Mysqldbs);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

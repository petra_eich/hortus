<?php
/**
 * @var \App\Model\Entity\Plant $plants
 * @var $pagecolor
 * @var $tblitems
 */
?>

<?= $this->element('formsearch'); ?>

<div class="card border-<?= $pagecolor; ?> mb-3">
    <?= $this->element('tablecontent', ['tbldata' => $plants, 'tblitems' => $tblitems,]); ?>

    <div class="card-footer border-<?= $pagecolor; ?>">
        <small class="text-muted"><?= $this->element('pagination'); ?></small>
    </div>
</div>

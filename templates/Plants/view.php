<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Plant $plant
 * @var  $tblitems
 * @var  $pagecolor
 * @var  $infolinks
 */
?>
<?= $this->element('formbody', ['formcontent' => $plant,]); ?>

<?php $id = $plant->id;

if ( count($infolinks) > 0 ) { ?>

    <div class="divider py-1 my-4 bg-<?= $pagecolor; ?>"></div>

    <div class="row">
        <div class="col">
            <h3 class="text-muted pb-3"><?= __('Related Links'); ?></h3>
        </div>
    </div>

    <div class="card border-<?= $pagecolor; ?> mb-3">

        <?= $this->element('tablecontent', ['tbldata' => $infolinks, 'tblitems' => $tblitems,]); ?>

        <div class="card-footer border-<?= $pagecolor; ?>">
            <small class="text-muted">
                <?= $this->element('pagination', ['model' => 'Infolinks']); ?>
            </small>
        </div>
    </div> <?php
} ?>

<?php
/**Plant
 * @var \App\Model\Entity\Plant $plant
 * @var  $tblitems
 * @var  $infolinks
 * @var  $pagecolor
 */
?>

<?= $this->element('formbody', ['formcontent' => $plant,]); ?>

<?php $id = $plant->id; ?>

    <div class="divider py-1 my-4 bg-<?= $pagecolor; ?>"></div>

<div class="row">
    <div class="col">
        <h3 class="text-muted pb-3"><?= __('Related Links'); ?></h3>
    </div>

    <div class="col text-right">
        <?= $this->Html->link( __('Add Link'),
            [
                'controller' => 'Infolinks',
                'action'     => 'add',
                $id,
            ],
            [
                'class' => 'btn btn-success',
                'type'  => 'button',
            ]
        ); ?>
    </div>
</div <?php

if ( count($infolinks) > 0 ) { ?>

    <div class="card border-<?= $pagecolor; ?> mb-3">

        <?= $this->element('tablecontent', ['tbldata' => $infolinks, 'tblitems' => $tblitems,]); ?>

        <div class="card-footer border-<?= $pagecolor; ?>">
            <small class="text-muted">
                <?= $this->element('pagination', ['model' => 'Infolinks']); ?>
            </small>
        </div>
    </div> <?php
}

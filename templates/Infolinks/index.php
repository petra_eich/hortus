<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Infolink $infolinks
 * @var $plant
 * @var $pagecolor
 * @var $tblitems
 */
?>

<?= $this->element('formsearch'); ?>

<div class="card border-<?= $pagecolor; ?> mb-3">

    <?= $this->element('tablecontent',['tbldata' => $infolinks, 'tblitems' => $tblitems,]); ?>

    <div class="card-footer border-<?= $pagecolor; ?>">
        <small class="text-muted"><?= $this->element('pagination'); ?></small>
    </div>
</div>

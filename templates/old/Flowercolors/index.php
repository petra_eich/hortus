<?php
/**
 * @var $pagecolor
 * @var $plantcolor
 * @var $tblitems
 */
?>
<div class="card border-<?= $pagecolor; ?> mb-3">

    <?= $this->element('tablecontent',['tbldata' => $flowercolors, 'tblitems' => $tblitems,]); ?>

    <div class="card-footer border-<?= $pagecolor; ?>">
        <small class="text-muted"><?= $this->element('pagination'); ?></small>
    </div>
</div>


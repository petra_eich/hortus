<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\old\Habitat $habitat
 * @var $pagecolor
 * @var $plantcolor
 * @var $plants
 * @var $tblitems
 */  ?>

<?= $this->element('formbody', ['formcontent' => $habitat,]); ?>

<div class="divider py-1 my-4 bg-<?= $pagecolor; ?>"></div> <?php

if ( count($plants) > 0 ) { ?>

    <h3 class="text-muted pb-3"><?= __('Related Plants'); ?></h3>

    <div class="card border-<?= $plantcolor; ?> mb-3">

        <?= $this->element('tablecontent',['tbldata' => $plants, 'tblitems' => $tblitems, 'tblcolor' => $plantcolor]); ?>

        <div class="card-footer border-<?= $plantcolor; ?>">
            <small class="text-muted"><?= $this->element('pagination'); ?></small>
        </div>
    </div> <?php
} else { ?>

    <h3 class="text-muted pb-3"><?= __( 'No Related Plant' ); ?></h3> <?php
} ?>

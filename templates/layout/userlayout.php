<?php
$cakeDescription = 'Hortus Jadestern'; ?>

<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <?= $this->Html->css([
        'bootstrap',
        'bootstrap-icons',
        'style',
        'normalize.min',
        'milligram.min',
        'cake',
    ]); ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?php $action = $this->request->getParam('action');?>
</head>
<body>
    <nav class="top-nav">
        <div class="top-nav-title">
            <h1><?= $cakeDescription; ?><?= $action == 'login' ? '-  LOGIN' : ''; ?></h1>
        </div>
    </nav>
    <main class="main">
        <div class="container">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>
    </main>
    <footer>
        <!--<div>Icons erstellt von <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/de/" title="Flaticon">www.flaticon.com</a></div>-->
    </footer>

    <?= $this->Html->script([
        'jquery.min', 
        'bootstrap.bundle', 
        'flash', 
    ]); ?>
    <?= $this->fetch('script') ?>
</body>
</html>

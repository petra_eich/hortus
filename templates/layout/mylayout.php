<?php
/**
 * @var \App\View\AppView $this
 * @var $pagetitle
 * @var $controller
 */
$cakeDescription = 'Hortus Jadestern'; ?>
<!DOCTYPE html>
<html lang="de">
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            <?= $cakeDescription . ': ' . $pagetitle; ?>
        </title>

        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">

        <?= $this->Html->meta('icon'); ?>
        <?= $this->Html->css([
            'bootstrap',
            'bootstrap-icons',
            'style',
        ]); ?>
        <?= $this->fetch('meta'); ?>
        <?= $this->fetch('css'); ?>
    </head>
    <body>
        <?= $this->element('navigation', ['controller' => $controller]); ?>

        <div id="contentcontainer"  class="container-fluid">
            <?= $this->element('contenttop', ['pagetitle' => $pagetitle]); ?>
            <?= $this->Flash->render(); ?>
            <div class="row">
                <div class="col-sm-2">
                    <?= $this->element('sidenav'); ?>
                </div>

                <div class="col-sm-10">
                    <?= $this->fetch('content'); ?>
                </div>
            </div>
        </div>

        <?= $this->element('footernav'); ?>

        <?= $this->Html->script([
            'jquery.min',
            'bootstrap.bundle',
            'flash',
        ]); ?>
        <?= $this->fetch('script'); ?>
    </body>
</html>

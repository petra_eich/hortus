
<div class="users form content">
    <?= $this->Form->create($user); ?>
    <fieldset> <?php
        echo $this->Form->control('username');
        if ($this->Form->isFieldError('username')) {
            echo $this->Form->error('username');
        }
        echo $this->Form->control('firstname');
        echo$this->Form->control('lastname');
        echo $this->Form->control('email');
        echo $this->Form->control('password'); ?>
    </fieldset>
    <?= $this->Form->button(__('Register'), [
        'type' => 'submit',
        'class' => 'btn btn-success',
    ]); ?>
    <?= $this->Form->end(); ?>

    <?= $this->Html->link('Login', ['action' => 'login']); ?>
</div>


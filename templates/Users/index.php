<div class="card border-warning mb-3">

    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead class="thead-light">
                <tr>
                    <th><?= $this->Paginator->sort('username'); ?></th>
                    <th><?= $this->Paginator->sort('fistname'); ?></th>
                    <th><?= $this->Paginator->sort('lastname'); ?></th>
                    <th><?= $this->Paginator->sort('email'); ?></th>
                    <th><?= $this->Paginator->sort('role'); ?></th>
                    <th><?= __('Change Status'); ?></th>
                    <th class="actions"><?= __('Actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user): ?>
                    <tr>
                        <?php $id = $user->id; ?>
                        <td><?= h($user->username); ?></td>
                        <td><?= h($user->firstname); ?></td>
                        <td><?= h($user->lastname); ?></td>
                        <td><?= h($user->email); ?></td>
                        <td><?= h($user->role); ?></td>
                        <td> <?php
                            if ( $user->status ) {

                                echo $this->Form->postLink(__('Inactive'),
                                    [
                                        'action' => 'userStatus',
                                        $user->id,
                                        $user->status,
                                    ],
                                    [
                                        'confirm' => __('Are you sure you want to active {0}?', $user->username),
                                        'class' => 'btn btn-danger',
                                        'type' => 'button',
                                    ]
                                );
                            } else {

                                echo $this->Form->postLink(__('Active'),
                                    [
                                        'action' => 'userStatus',
                                        $user->id,
                                        $user->status,
                                    ],
                                    [
                                        'confirm' => __('Are you sure you want to inactive {0}?', $user->username),
                                        'class' => 'btn btn-success',
                                        'type' => 'button',
                                    ]
                                );
                            } ?>
                        </td>
                        <td class="actions">
                            <?= $this->Html->link(
                                __('Details'),
                                ['action' => 'view', $id],
                                [
                                    'class' => 'btn btn-secondary',
                                    'type' => 'button',
                                ]
                            ); ?>
                            <?= $this->Html->link(
                                __('Edit'),
                                ['action' => 'edit', $id],
                                [
                                    'class' => 'btn btn-primary',
                                    'type' => 'button',
                                ]
                            ); ?>
                            <?= $this->Form->postLink(
                                __('Delete'),
                                ['action' => 'delete', $id],
                                [
                                    'confirm' => __('Are you sure you want to delete {0}?', $user->username),
                                    'class' => 'btn btn-danger',
                                    'type' => 'button',
                                ]
                            ); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <div class="card-footer border-warning">
        <small class="text-muted"><?= $this->element('pagination'); ?></small>
    </div>
</div>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Family $family
 * @var $pagecolor
 * @var $plantcolor
 * @var $plants
 * @var $tblitems
 * @var $genera
 * @var $gentblitems
 */
?>

<?= $this->element('formbody', ['formcontent' => $family]); ?> <?php

if ( count($genera) > 0 ) { ?>

    <div class="divider py-1 my-4 bg-<?= $pagecolor; ?>"></div>

    <h3 class="text-muted pb-3"><?= __('Related Plant Genera'); ?></h3>

    <div class="card border-<?= $pagecolor; ?> mb-3">

        <?= $this->element('tablecontent',[
                'tbldata' => $genera,
                'tblitems' => $gentblitems,
                'tblcolor' => $pagecolor,
                'model' => 'Genera',
                'filter' => ['genus' => [$family->id]],
            ]); ?>

        <div class="card-footer border-<?= $pagecolor; ?>">
            <small class="text-muted"><?= $this->element('pagination'); ?></small>
        </div>
    </div> <?php
}

if ( count($plants) > 0 ) { ?>

    <div class="divider py-1 my-4 bg-<?= $pagecolor; ?>"></div>

    <h3 class="text-muted pb-3"><?= __('Related Plants'); ?></h3>

    <div class="card border-<?= $plantcolor; ?> mb-3">

        <?= $this->element('tablecontent',[
                'tbldata' => $plants,
                'tblitems' => $tblitems,
                'tblcolor' => $plantcolor,
                'model' => 'Plants',
                'filter' => ['plant' => [$family->id]],
        ]); ?>

        <div class="card-footer border-<?= $plantcolor; ?>">
            <small class="text-muted"><?= $this->element('pagination'); ?></small>
        </div>
    </div> <?php
} ?>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Family $family
 */ ?>

<?= $this->element('formbody', ['formcontent' => $family,]); ?>

<?php
/**
 * @var $model
 */

//if ( isset($model) ) {
//    $this->Paginator->options(['model' => $model]);
//}
?>

<ul class="pagination">
    <?= $this->Paginator->first(__('First', TRUE),
        array('tag' => 'li', 'escape' => FALSE),
        array('type' => 'button', 'class' => 'btn btn-default')); ?>
    <!--    --><?php
    //    if ( $this->Paginator->hasprev ) { ?>

    <?=  $this->Paginator->prev('&laquo;',
        array('tag' => 'li', 'escape' => FALSE),
        array('class' => 'prev disabled', 'tag' => 'li', 'escape' => FALSE)); ?>
    <!--        --><?php
    //    } ?>
    <?=  $this->Paginator->numbers(array(
        'separator' => '',
        'tag' => 'li',
        'currentLink' => TRUE,
        'currentClass' => 'active',
        'currentTag' => 'a'
    )); ?>

    <!--    --><?php
    //    if ( $this->Paginator->hasnext ) { ?>

    <?=  $this->Paginator->next('&raquo;',
        array('tag' => 'li', 'escape' => FALSE),
        array('class' => 'prev disabled', 'tag' => 'li', 'escape' => FALSE)); ?>
    <!--        --><?php
    //    } ?>
    <?=  $this->Paginator->last(__('Last', TRUE),
        array('tag' => 'li', 'escape' => FALSE),
        array('type' => 'button', 'class' => 'btn btn-default')); ?>
</ul>
<p>
    <?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')); ?>
</p>

<?php
/**
 * @var $pagecolor
 * @var $pagetitle
 * @var $toptitle
 */ ?>

<div class="jumbotron rounded bg-<?= __($pagecolor); ?> text-white">
    <h1 class="display-4">
        <?= __($pagetitle); ?>
        <?= isset($toptitle) ? ' -> ' . $toptitle : ''; ?>
    </h1>
</div>


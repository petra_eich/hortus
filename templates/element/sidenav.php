<?php
/**
 * @var $pagecolor
 * @var $sidenavitems
 */
$actions = [
    'index' => [
        'name' => 'List',
        'btncolor' => $pagecolor,
    ],
    'view' => [
        'name' => 'Detail',
        'btncolor' => $pagecolor,
    ],
    'edit' => [
        'name' => 'Edit',
        'btncolor' => 'primary',
    ],
    'add' => [
        'name' => 'Add',
        'btncolor' => 'success',
    ],
    'delete' => [
        'name' => 'Delete',
        'btncolor' => 'danger',
    ],
]; ?>

<h1>Actions</h1>

<nav class="nav flex-column"> <?php

    foreach ($sidenavitems as $item) {

        $btncolor = isset($item['btncolor']) ?  $item['btncolor'] : $actions[$item['action']]['btncolor'];
        $btnclass = 'nav-link btn my-2 text-white btn-' . $btncolor;
        $btnname = isset($item['name']) ? $item['name'] : $actions[$item['action']]['name'];
        
        if ( isset($item['param'])) {
            
            $btnurl = [
                'controller' => isset($item['controller']) ? $item['controller'] : $this->request->getParam('controller'),
                'action' => $item['action'],
                $item['param'],
            ];
        } else {
            
            $btnurl = [
                'controller' => isset($item['controller']) ? $item['controller'] : $this->request->getParam('controller'),
                'action' => $item['action'],
            ];
        }

        if ( array_key_exists('postlink', $item) ) {

            echo $this->Form->postLink(
                $btnname,
                $btnurl,
                [
                    'confirm' =>  $item['postlink'],
                    'class' => $btnclass,
                    'type' => 'button',
                ]
            );
        } else {
            
            echo $this->Html->link(
                $btnname,
                $btnurl,
                [
                    'type' => 'button',
                    'class' => $btnclass,
                ]
            );
        }
    } ?>
</nav>

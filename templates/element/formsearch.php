<?= $this->Form->create(null, ['type' => 'get']); ?>
    <div class="card border-<?= $pagecolor; ?> mb-3">
        <div class="card-body text-dark">
            <div class="form-group row">
                <div class="col-sm-4">
                    <?= $this->Form->control('key', [
                        'id' => 'key',
                        'value' => $this->request->getQuery('key'),
                        'class' => 'form-control my-2',
                        'label' => FALSE,
                    ]); ?>
                </div>
                <div class="col-sm-4">
                    <?= $this->Form->button(__('Search'), [
                        'id' => 'btnSubmit',
                        'type' => 'submit',
                        'class' => 'btn btn-primary my-2',
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
<?= $this->Form->end(); ?>

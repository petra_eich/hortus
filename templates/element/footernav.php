<?php
use Cake\Core\Configure;
?>
<footer>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-bottom">
      <span class="navbar-text">
          <?= $this->Identity->isLoggedIn() ? 'logged in as ' . $this->Identity->get('username') : ''; ?> - CakePHP <?= Configure::version(); ?> / PHP <?= PHP_VERSION; ?>
      </span>
    </nav>
<!--    <div>Icons erstellt von <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/de/" title="Flaticon">www.flaticon.com</a></div>-->
</footer>


<?php
/**
 * @var $controller
 */ ?>

<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="nav navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item<?= $controller == 'Plants' ? ' active' : '' ?>">
                <h3>
                    <a class="nav-link" href="<?= $this->Url->build(
                        [
                            'controller' => 'Plants',
                            'action' => 'index',
                        ]); ?>">
                        Plants
                    </a>
                </h3>
            </li>
        </ul>

        <ul class="nav navbar-nav ms-auto mb-2 mb-lg-0">

            <li class="nav-item<?= $controller == 'Families' ? ' active' : '' ?>">
                <a class="nav-link" href="<?= $this->Url->build(
                    [
                        'controller' => 'Families',
                        'action' => 'index',
                    ]); ?>">
                    Families
                </a>
            </li>

            <li class="nav-item<?= $controller == 'Genera' ? ' active' : '' ?>">
                <a class="nav-link" href="<?= $this->Url->build(
                    [
                        'controller' => 'Genera',
                        'action' => 'index',
                    ]); ?>">
                    Plant Genera
                </a>
            </li>

            <li class="nav-item<?= $controller == 'Infolinks' ? ' active' : '' ?>">
                <a class="nav-link" href="<?= $this->Url->build(
                    [
                        'controller' => 'Infolinks',
                        'action' => 'index',
                    ]); ?>">
                    Links
                </a>
            </li> <?php

            if ( $this->Identity->get('role') == 'admin' ) { ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?= $this->Url->build(
                        [
                            'controller' => 'Users',
                            'action' => 'index',
                        ]); ?>">
                        User
                    </a>
                </li> <?php
            }

            if ( $this->Identity->isLoggedIn() ) { ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?= $this->Url->build(
                        [
                            'controller' => 'Users',
                            'action' => 'logout',
                        ]); ?>">
                        Logout
                    </a>
                </li><?php
            } ?>
        </ul>
    </div>
</nav>

